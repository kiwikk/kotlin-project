package com.kiwikk.myawesomeproject.elements

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.kiwikk.myawesomeproject.R

//именование вьюихи вьюхой

class New : LinearLayout {
    var view: View? = null
    var textView: TextView? = null

    constructor(context: Context?, view: View?) : super(context) {
        init(context)
        //setSaveEnabled(true);
        this.view = view
    }

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context)
    }

    private fun init(context: Context?) {
        (context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.notif_layout, this, true)
        orientation = VERTICAL
    }

    fun setText(text: String?) {
        textView = findViewById<TextView?>(R.id.notifsText)
        textView!!.setText(text)
    }
}