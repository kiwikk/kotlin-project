package com.kiwikk.myawesomeproject.database

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.kiwikk.myawesomeproject.person.Person

class DBRepository(context: Context) {
    private val dbHelper = DataBaseHelper(context)
    private val readableDB = dbHelper.readableDatabase
    private val writableDB = dbHelper.writableDatabase

    fun findPerson(): Person? {
        var person: Person? = null
        val cursor = openCursor()

        if (cursor!!.moveToFirst()) {
            val idIndex = cursor.getColumnIndex(DataBaseHelper.ID)
            val nameIndex = cursor.getColumnIndex(DataBaseHelper.NAME)
            val bDateIndex = cursor.getColumnIndex(DataBaseHelper.DATE_OF_BIRTH)

            Log.d("Colums:", "id: ${idIndex}, name: ${nameIndex}, bDate: ${bDateIndex}")
            Log.d("Data:", "id: ${cursor.getInt(idIndex)}, name: ${cursor.getString(nameIndex)}, bDate: ${cursor.getString(bDateIndex)}")

            person = Person(cursor.getInt(idIndex), cursor.getString(nameIndex), cursor.getString(bDateIndex))
            cursor.close()
        }

        return person
    }

    private fun openCursor(): Cursor? {
        return readableDB.query("PERSON", arrayOf<String?>("_id", "NAME", "DATE_OF_BIRTH"),
                "_id = 1", null,
                null, null, null)
    }

    fun insertPerson(person: Person) {
        dbHelper.insertPerson(person, writableDB)
    }

    fun updateDB(person: Person, column: String) {
        dbHelper.updateDB(person, writableDB, column)
    }

    fun closeDB() {
        readableDB.close()
        writableDB.close()
    }

}