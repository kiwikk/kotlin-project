package com.kiwikk.myawesomeproject.fragments.settings

import com.kiwikk.myawesomeproject.AndroidApplication
import com.kiwikk.myawesomeproject.database.DBRepository
import com.kiwikk.myawesomeproject.database.DataBaseHelper
import com.kiwikk.myawesomeproject.person.Person
import javax.inject.Inject

class SettingsInteractor {
    @Inject
    lateinit var db : DBRepository
    var person: Person?

    init {
        AndroidApplication.component.inject(this)
        person = db.findPerson()
    }

    private fun updateDB(column: String) {
        db.updateDB(person!!, column)
    }

    fun updateName(name: String) {
        person = db.findPerson()
        person?.name = name
        updateDB(DataBaseHelper.NAME)
    }

    fun updateBDate(bDate: String) {
        person = db.findPerson()
        person?.birthDate = bDate
        updateDB(DataBaseHelper.DATE_OF_BIRTH)
    }

    fun getPersonName(): String {
        person = db.findPerson()
        return person?.name ?: "No name"
    }
}