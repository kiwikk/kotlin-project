package com.kiwikk.myawesomeproject.fragments.notifications

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndStrategy::class)
interface NotifsView : MvpView {
    fun addNews(text: String)
}