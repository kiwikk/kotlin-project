package com.kiwikk.myawesomeproject.components

import com.kiwikk.myawesomeproject.fragments.home.HomeModule
import com.kiwikk.myawesomeproject.fragments.home.HomePresenter
import com.kiwikk.myawesomeproject.fragments.notifications.NotifsModule
import com.kiwikk.myawesomeproject.fragments.notifications.NotifsPresenter
import com.kiwikk.myawesomeproject.fragments.settings.SettingsModule
import com.kiwikk.myawesomeproject.fragments.settings.SettingsPresenter
import com.kiwikk.myawesomeproject.scopes.MainActivityScope
import dagger.Component

@Component(modules = [HomeModule::class, NotifsModule::class, SettingsModule::class])
@MainActivityScope
interface ActivityComponent {
    fun inject(hPresenter: HomePresenter)
    fun inject(nPresenter: NotifsPresenter)
    fun inject(sPresenter: SettingsPresenter)
}