package com.kiwikk.myawesomeproject.fragments.home

import android.content.SharedPreferences
import com.kiwikk.myawesomeproject.MainActivity
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class HomePresenter: MvpPresenter<HomeView>() {
    @Inject
    lateinit var homeInteractor: HomeInteractor
    //TODO: INJECT?
   lateinit var sharedPreferences: SharedPreferences

    init {
        MainActivity.component.inject(this)
    }


    var name: String = ""
    var bDate: String = ""
    var id = 1

    fun setSharedPrefs(sp: SharedPreferences) {
        sharedPreferences = sp
    }

    fun firstRun() {
        if (sharedPreferences.getBoolean("firstrun", true)) {
            viewState.setPersonName()
            sharedPreferences.edit().putBoolean("firstrun", false).apply()
        }
    }

    fun setPersonBDate() {
        viewState.setPersonBDate()
    }

    fun insertPerson() {
        homeInteractor.insertPersonToDB(id++, name, bDate)
    }

    fun createCalendar() {
        val weeks = homeInteractor.getWeeks()
        viewState.createTable(weeks)
        colorWeeks()
    }

    fun colorWeeks() {
        val weeks = homeInteractor.person?.weeks ?: 0
        viewState.colorWeeks(weeks)
    }

    fun closeDB() {
        homeInteractor.closeDB()
    }
}