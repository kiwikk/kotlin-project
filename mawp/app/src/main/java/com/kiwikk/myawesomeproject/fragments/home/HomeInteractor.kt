package com.kiwikk.myawesomeproject.fragments.home

import com.kiwikk.myawesomeproject.AndroidApplication
import com.kiwikk.myawesomeproject.MainActivity
import com.kiwikk.myawesomeproject.database.DBRepository
import com.kiwikk.myawesomeproject.person.Person
import javax.inject.Inject

class HomeInteractor {
    @Inject
    lateinit var db: DBRepository
    var person:Person?

    init {
        AndroidApplication.component.inject(this)
        person = db.findPerson()
    }


    fun insertPersonToDB(_id: Int, name: String, bDate: String) {
        db.insertPerson(Person(_id, name, bDate))
        person = db.findPerson()
    }

    fun closeDB() {
        db.closeDB()
    }

    fun getWeeks():Int{
        person = db.findPerson()
        return person?.weeks ?: 0
    }
}