package com.kiwikk.myawesomeproject

import android.app.Application
import android.util.Log
import com.kiwikk.myawesomeproject.components.AppComponent
import com.kiwikk.myawesomeproject.components.DaggerAppComponent
import com.kiwikk.myawesomeproject.database.DBRepModule
import com.kiwikk.myawesomeproject.main.ContextModule

class AndroidApplication : Application() {
    companion object {
        lateinit var component: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        component = buildComponent()
    }

    protected fun buildComponent(): AppComponent {
        Log.d("apContext: ", (applicationContext==null).toString())
        return DaggerAppComponent.builder()
                .contextModule(ContextModule(applicationContext))
                .dBRepModule(DBRepModule())
                .build()
    }
}