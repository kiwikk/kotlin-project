package com.kiwikk.myawesomeproject.fragments.notifications

import com.kiwikk.myawesomeproject.AndroidApplication
import com.kiwikk.myawesomeproject.database.DBRepository
import com.kiwikk.myawesomeproject.person.Person
import javax.inject.Inject


class NotifsInteractor {
    @Inject
    lateinit var db: DBRepository
    var person: Person?

    init {
        AndroidApplication.component.inject(this)
        person = db.findPerson()
    }

    fun getPersonAge(): Int {
        person = db.findPerson()
        return person!!.weeks/52
    }
}