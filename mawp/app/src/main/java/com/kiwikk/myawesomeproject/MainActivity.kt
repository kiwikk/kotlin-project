package com.kiwikk.myawesomeproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.ismaeldivita.chipnavigation.ChipNavigationBar
import com.kiwikk.myawesomeproject.components.ActivityComponent
import com.kiwikk.myawesomeproject.components.DaggerActivityComponent
import com.kiwikk.myawesomeproject.fragments.home.HomeFragment
import com.kiwikk.myawesomeproject.fragments.home.HomeModule
import com.kiwikk.myawesomeproject.fragments.notifications.NotificationsFragment
import com.kiwikk.myawesomeproject.fragments.notifications.NotifsModule
import com.kiwikk.myawesomeproject.fragments.settings.SettingsFragment
import com.kiwikk.myawesomeproject.fragments.settings.SettingsModule
import com.kiwikk.myawesomeproject.main.MainPresenter
import com.kiwikk.myawesomeproject.main.MainView
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), MainView {
    companion object {
        lateinit var component: ActivityComponent
            private set
    }

    private var chipNavigationBar: ChipNavigationBar? = null
    private lateinit var fragment: Fragment

    lateinit var homeFragment: HomeFragment
    lateinit var notificationsFragment: NotificationsFragment
    lateinit var settingsFragment: SettingsFragment


    @InjectPresenter
    lateinit var mPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        initFragments()
        component = buildComponent()

        chipNavigationBar = findViewById<ChipNavigationBar?>(R.id.chipNavigation)
        chipNavigationBar?.setItemSelected(R.id.home, true)

        chipNavigationBar?.setOnItemSelectedListener(object : ChipNavigationBar.OnItemSelectedListener {
            override fun onItemSelected(i: Int) {
                when (i) {
                    R.id.home -> mPresenter.beginHomeTransaction()
                    R.id.notifications -> mPresenter.beginNotifsTransaction()
                    R.id.settings -> mPresenter.beginSettingsTransaction()
                }
            }
        })
    }

    fun initFragments() {
        homeFragment = HomeFragment()
        notificationsFragment = NotificationsFragment()
        settingsFragment = SettingsFragment()
    }

    private fun buildComponent(): ActivityComponent {
        return DaggerActivityComponent.builder()
                .homeModule(HomeModule())
                .notifsModule(NotifsModule())
                .settingsModule(SettingsModule())
                .build()
    }

    override fun beginHomeTransaction() {
        supportFragmentManager.beginTransaction().replace(R.id.container, homeFragment).commit()
    }

    override fun beginNotifsTransaction() {
        supportFragmentManager.beginTransaction().replace(R.id.container, notificationsFragment).commit()
    }

    override fun beginSettingsTransaction() {
        supportFragmentManager.beginTransaction().replace(R.id.container, settingsFragment).commit()
    }
}