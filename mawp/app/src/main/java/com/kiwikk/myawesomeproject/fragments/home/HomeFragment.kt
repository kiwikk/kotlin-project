package com.kiwikk.myawesomeproject.fragments.home

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.elements.WeekButton
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import android.util.Log
import java.util.*

//добавить mvp, интерракторы
//создавать в презентере новый интеррактор
//в интерракторе должны быть данные, которые юзает фрагмент
//нужен репозиторий, репозиторий обращается к базе данных и возвращает данные
//репозиторием пользуется только интеррактор
//сделать поле интеррактора в презенторе и в ините создавать

//презентер ходит в интеррактор за данными
//либо брать контест из вьюхи, либо через презентер


//разбить на интеракторы
//добавить класс андроид апликейшн
//добавить зависимости новых интеракторов, репозиториев и тд, распределено по компонентам должно быть
//юзать их во фрагментах, идти в дагер забирать презентер оттуда
//презентеры, интеракторы и тд должны юзать дагер в конструкторе
//сделать как минимум 2 компонента (апликейшн и активити)

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : MvpAppCompatFragment(), HomeView {
    private lateinit var vview: View

    @InjectPresenter
    lateinit var homePresenter: HomePresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homePresenter.setSharedPrefs(requireActivity().getSharedPreferences("com.kiwikk.myawesomeproject", Context.MODE_PRIVATE))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        vview = inflater.inflate(R.layout.fragment_home, container, false)

        homePresenter.firstRun()
        homePresenter.createCalendar()

        return vview
    }

    //обычно юзается грид
    override fun createTable(weeks: Int) {
        val tableLayout = vview.findViewById<TableLayout?>(R.id.tableLayout)
        for (i in 0..WEEK_ROWS) {
            val tableRow = TableRow(this.context)
            for (j in 0..WEEK_COLUMNS) {
                if (i == 0) {
                    val textView = TextView(this.context)
                    if (j != 0 && j % 10 == 0 || j == 1) textView.text = j.toString() else textView.text = ""
                    textView.setPadding(20, 0, 0, 0)
                    tableRow.addView(textView)
                    continue
                }

                val id = (i - 1) * 52 + j
                //TODO: посмотри что со мной можно сделать (weekButton)
                val weekButton = WeekButton(this.context, id, fragmentManager)
                if (id < weeks) weekButton.setLived()
                Log.d("Creating week", "Weeks: ${weeks}")

                val p = TableRow.LayoutParams(70, 70)
                p.setMargins(-5, 0, -5, 0)
                weekButton.layoutParams = p

                if (j == 0) {
                    val textView = TextView(this.context)
                    if (i % 10 == 0 || i == 1) textView.text = i.toString() else textView.text = ""
                    val params = TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT)
                    params.setMargins(0, -15, 10, -15)
                    textView.layoutParams = params
                    tableRow.addView(textView)
                    continue
                }
                val params = TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
                params.setMargins(0, -10, 25, -10)
                tableRow.layoutParams = params
                tableRow.addView(weekButton, j)
            }
            tableLayout.addView(tableRow, i)
        }
    }

    override fun colorWeeks(weeks: Int) {
        val tableLayout = vview.findViewById<TableLayout?>(R.id.tableLayout)
        for (i in 1..WEEK_ROWS) {
            val tableRow = tableLayout.getChildAt(i) as TableRow
            for (j in 1..WEEK_COLUMNS) {
                val weekButton = tableRow.getChildAt(j) as WeekButton
                if (weekButton.getID() < weeks) weekButton.setLived()
            }
        }
    }

    override fun setPersonName() {
        val alertBuilder = AlertDialog.Builder(this.context)
        alertBuilder.setTitle("Давай знакомиться")
        alertBuilder.setMessage("Я - твой мотивационный помощник Мотя, а ты?")
        val input = EditText(this.context)
        alertBuilder.setView(input)

        var name = ""
        alertBuilder.setPositiveButton("Продолжим") { dialog, which ->
            name = input.text.toString()
        }
        alertBuilder.setNegativeButton("Отстань") { dialog, which ->
            name = "Вася"
            Toast.makeText(context,
                    "Ну нет так нет, теперь ты - " + name[0] + ".", Toast.LENGTH_LONG).show()
        }

        alertBuilder.setOnDismissListener {
            homePresenter.name = name
            homePresenter.setPersonBDate()
        }
        val ad = alertBuilder.create()
        ad.show()
    }

    override fun setPersonBDate() {
        val calendar = Calendar.getInstance()
        val mYear = calendar[Calendar.YEAR]
        val mMonth = calendar[Calendar.MONTH]
        val mDay = calendar[Calendar.DAY_OF_MONTH]
        val date = StringBuilder()

        //val db = dbHelper!!.writableDatabase
        val datePickerDialog = DatePickerDialog(context!!, AlertDialog.THEME_HOLO_LIGHT,
                { _, year, month, dayOfMonth ->
                    if (dayOfMonth.toString().length == 1) date.append(0)
                    date.append(dayOfMonth).append(" ")
                    if ((month + 1).toString().length == 1) date.append(0)
                    date.append(month + 1).append(" ").append(year)
                    homePresenter.bDate = date.toString()
                    homePresenter.insertPerson()
                    homePresenter.colorWeeks()
                    //person!!.setBirthDate(date.toString())
                    //colorWeeks()
                    // homePresenter.colorWeeks()
                    //dbHelper!!.insertPerson(person, db)
                }, mYear, mMonth, mDay)
        datePickerDialog.setTitle("А теперь мне нужна твоя дата рождения")
        datePickerDialog.setCancelable(false)
        datePickerDialog.show()

    }
    

    companion object {
        private const val WEEK_ROWS = 80 //80
        private const val WEEK_COLUMNS = 52 //52


        fun newInstance(): HomeFragment {
            val fragment = HomeFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}