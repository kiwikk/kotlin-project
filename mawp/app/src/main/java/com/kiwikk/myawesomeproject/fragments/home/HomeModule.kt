package com.kiwikk.myawesomeproject.fragments.home

import com.kiwikk.myawesomeproject.scopes.MainActivityScope
import dagger.Module
import dagger.Provides

@Module
class HomeModule {
    val homeInteractor = HomeInteractor()

    @Provides
    @MainActivityScope
    fun provideHomeInteractor() = homeInteractor
}