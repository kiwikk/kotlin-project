package com.kiwikk.myawesomeproject.database
import android.content.Context
import com.kiwikk.myawesomeproject.main.ContextModule
import com.kiwikk.myawesomeproject.scopes.ApplicationScope
import dagger.Module;
import dagger.Provides;

@Module(includes = [ContextModule::class])
class DBRepModule {
    @Provides
    @ApplicationScope
    fun provideDB(context: Context) = DBRepository(context)
}
