package com.kiwikk.myawesomeproject.components

import com.kiwikk.myawesomeproject.database.DBRepModule
import com.kiwikk.myawesomeproject.fragments.home.HomeInteractor
import com.kiwikk.myawesomeproject.fragments.notifications.NotifsInteractor
import com.kiwikk.myawesomeproject.fragments.settings.SettingsInteractor
import com.kiwikk.myawesomeproject.scopes.ApplicationScope
import dagger.Component

@Component(modules = [DBRepModule::class])
@ApplicationScope
interface AppComponent {
    fun inject(homeInteractor: HomeInteractor)
    fun inject(notifsInteractor: NotifsInteractor)
    fun inject(settingsInteractor: SettingsInteractor)
}

