package com.kiwikk.myawesomeproject.fragments.notifications

import com.kiwikk.myawesomeproject.scopes.MainActivityScope
import dagger.Module
import dagger.Provides

@Module
class NotifsModule {
    val nInteractor = NotifsInteractor()

    @Provides
    @MainActivityScope
    fun provideNotifsInteractor() = nInteractor
}