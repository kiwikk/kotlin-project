package com.kiwikk.myawesomeproject.fragments.settings

import com.kiwikk.myawesomeproject.scopes.MainActivityScope
import dagger.Module
import dagger.Provides

@Module
class SettingsModule {
    val sInteractor = SettingsInteractor()

    @Provides
    @MainActivityScope
    fun provideSettingsInteractor() = sInteractor
}