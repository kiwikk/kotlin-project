package com.kiwikk.myawesomeproject.person

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

//TODO: сделать модел (на потом)
data class Person(val _id: Int,
                  var name: String,
                  var birthDate: String) {
    var birthDateLocal: LocalDate
    var weeks: Int
    private set

    init {
        val dtf = DateTimeFormatter.ofPattern("dd MM yyyy")
        birthDateLocal = LocalDate.parse(birthDate, dtf)

        val now = LocalDate.now()
        weeks = ChronoUnit.WEEKS.between(birthDateLocal, now).toInt()
    }
}