package com.kiwikk.myawesomeproject.fragments.settings

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import com.kiwikk.myawesomeproject.R
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import java.util.*


//данными занимается интерактор (Model в MVP)
/**
 * A simple [Fragment] subclass.
 * Use the [SettingsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SettingsFragment : MvpAppCompatFragment(), SettingsView {
    lateinit var vview: View
    lateinit var inflater: LayoutInflater
    lateinit var getDonutDialog: AlertDialog

    @InjectPresenter
    lateinit var sPresenter: SettingsPresenter



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        this.inflater = inflater
        vview = inflater.inflate(R.layout.fragment_settings, container, false)

        setButtons()
        return vview
    }

    private fun setButtons() {
        val changeNameButton = vview.findViewById<Button>(R.id.change_name)
        val changeBDateButton = vview.findViewById<Button>(R.id.change_bday)
        val notiffs = vview.findViewById<SwitchCompat>(R.id.notifs)
        val getDonut = vview.findViewById<Button>(R.id.getDonut)

        changeNameButton.setOnClickListener { sPresenter.getNewName() }
        changeBDateButton.setOnClickListener { sPresenter.setNewBDate() }
        setNotifs(notiffs)
        getDonut.setOnClickListener { sPresenter.showDonut() }
        createGetDonutAlert()
    }

    //dialogFragment вместо alertDialog
    //не очень хорошо юзать бд, по-хорошему юзать презентер
    //паблик, часть интерфейса вьюхи, юзать везде презентер на сетах
    override fun setNewName() {
        val alertBuilder = AlertDialog.Builder(this.context)
        alertBuilder.setTitle("Так ты не " + sPresenter.getPersonName() + "???")
        alertBuilder.setMessage("А я всё ещё Мотя..")

        val input = EditText(this.context)
        alertBuilder.setView(input)
        var name: String

        alertBuilder.setPositiveButton("Продолжим") { _, _ ->
            name = input.text.toString()
            Toast.makeText(context,
                    "Допустим", Toast.LENGTH_LONG).show()

            sPresenter.updateName(name)
        }

        alertBuilder.setNegativeButton("Да " + sPresenter.getPersonName() + " я, " + sPresenter.getPersonName()+ "..."
        ) { _, _ ->
            Toast.makeText(context,
                    "Смотри мне -.-", Toast.LENGTH_LONG).show()
        }

        val ad = alertBuilder.create()
        ad.show()
    }

    override fun setNewBDate() {
        val calendar = Calendar.getInstance()
        val mYear = calendar[Calendar.YEAR]
        val mMonth = calendar[Calendar.MONTH]
        val mDay = calendar[Calendar.DAY_OF_MONTH]
        val date = StringBuilder()

        val datePickerDialog = DatePickerDialog(context!!, AlertDialog.THEME_HOLO_LIGHT,
                { _, year, month, dayOfMonth ->
                    if (dayOfMonth.toString().length == 1) date.append(0)
                    date.append(dayOfMonth).append(" ")
                    if ((month + 1).toString().length == 1) date.append(0)
                    date.append(month + 1).append(" ").append(year)

                    sPresenter.updateBDate(date.toString())
                }, mYear, mMonth, mDay)

        datePickerDialog.setTitle("Ну меняй, меняй")
        datePickerDialog.setCancelable(false)
        datePickerDialog.show()
    }

    private fun setNotifs(notiffs: SwitchCompat) {
        notiffs.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                notiffs.text = "Включить уведомления"
                Toast.makeText(context,
                        "Уведомления отключены", Toast.LENGTH_LONG).show()
            } else {
                notiffs.text = "Отключить уведомления"
                Toast.makeText(context,
                        "Уведомления включены", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun createGetDonutAlert() {
        val builder = AlertDialog.Builder(context)
        val dInf = inflater.inflate(R.layout.donut_layout, null)

        builder.setView(dInf)
        builder.setTitle("Держи :з")

        builder.setPositiveButton("Спасибо") { _, _ ->
            Toast.makeText(context,
                    "Обращайся))", Toast.LENGTH_LONG).show()
        }

        builder.setNegativeButton("Нет ") { _, _ ->
            Toast.makeText(context,
                    "Ну нет так нет", Toast.LENGTH_LONG).show()
        }

        getDonutDialog = builder.create()
        getDonutDialog.setOnDismissListener { sPresenter.hideDonut() }
    }

    override fun showDonut() {
        getDonutDialog.show()
    }

    override fun hideDonnut() {
        getDonutDialog.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()

        getDonutDialog.setOnDismissListener(null)
        getDonutDialog.dismiss()
    }


    companion object {
        private const val ARG_PARAM1: String = "param1"
        private const val ARG_PARAM2: String = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SettingsFragment.
         */
        fun newInstance(param1: String?, param2: String?): SettingsFragment {
            val fragment = SettingsFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}