package com.kiwikk.myawesomeproject.fragments.settings

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface SettingsView: MvpView {
    fun showDonut()
    fun hideDonnut()
    fun setNewName()
    fun setNewBDate()
}