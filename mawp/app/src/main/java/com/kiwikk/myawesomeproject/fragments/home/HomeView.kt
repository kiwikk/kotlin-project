package com.kiwikk.myawesomeproject.fragments.home

import android.content.Context
import android.support.v4.media.session.PlaybackStateCompat
import com.kiwikk.myawesomeproject.person.Person
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface HomeView : MvpView {
    fun createTable(weeks:Int)
    fun colorWeeks(weeks: Int)
    fun setPersonName()
    fun setPersonBDate()
}