package com.kiwikk.myawesomeproject.elements

import android.content.Context
import android.graphics.Paint
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.LinearLayout
import com.kiwikk.myawesomeproject.R

class Task : LinearLayout {
    private var text: String? = null
    private val isDone = false
    private val idd = 0
    private var deleteButton: ImageButton? = null
    private var view: View? = null
    private var checkBox: CheckBox? = null

    constructor(context: Context?, view: View?) : super(context) {
        init(context)
        isSaveEnabled = true
        this.view = view
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context)
    }

    private fun init(context: Context?) {
        (context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.task_layout, this, true)
        orientation = VERTICAL
    }

    //хорошо впилить mvp
    fun setText(text: String?) {
        this.text = text
        checkBox = findViewById<CheckBox>(R.id.checkBox)
        checkBox!!.setText(text)
        deleteButton = findViewById<ImageButton?>(R.id.deleteButton)
        setDeleteListener(deleteButton)
    }

    //предоставить возможность сетить что-то своё
    // setOnDeleteClickListener
    private fun setDeleteListener(button: ImageButton?) {
        button!!.setOnClickListener(OnClickListener {
            checkBox!!.setPaintFlags(checkBox!!.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
            checkBox!!.setEnabled(false)
            button.setEnabled(false)
        })
    }

    private inner class SavedState : BaseSavedState {
        constructor(source: Parcel?) : super(source) {}
        constructor(source: Parcel?, loader: ClassLoader?) : super(source, loader) {}
        constructor(superState: Parcelable?) : super(superState) {}
    }
}