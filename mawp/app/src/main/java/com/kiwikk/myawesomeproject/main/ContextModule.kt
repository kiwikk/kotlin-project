package com.kiwikk.myawesomeproject.main

import android.content.Context
import android.util.Log
import com.kiwikk.myawesomeproject.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class ContextModule(val ctx: Context) {
    val context = ctx

    @Provides
    @ApplicationScope
    fun provideContext():Context {
        Log.d("apContext: ", context.toString())
        return context
    }
}