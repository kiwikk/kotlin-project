package com.kiwikk.myawesomeproject.elements

import android.content.Context
import androidx.appcompat.widget.SwitchCompat
import com.kiwikk.myawesomeproject.R

//селектор


class DaySwitch(context: Context, var idd: Int) : SwitchCompat(context) {
    var state = false
    private fun setListeners() {}
    override fun setChecked(checked: Boolean) {
        super.setChecked(checked)
        val color: Int
        val id: Int
        if (checked) {
            color = R.color.yellow_happy
            id = R.drawable.ic_ok
        } else {
            color = R.color.dark_gray
            id = R.drawable.ic_not_ok
        }
        setThumbResource(id)
    }

    override fun getId(): Int {
        return idd
    }

    init {
        setListeners()
    }
}