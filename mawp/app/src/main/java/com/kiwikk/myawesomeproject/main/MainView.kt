package com.kiwikk.myawesomeproject.main

import androidx.fragment.app.Fragment
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView:MvpView {
    fun beginHomeTransaction()
    fun beginNotifsTransaction()
    fun beginSettingsTransaction()
}