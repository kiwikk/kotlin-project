package com.kiwikk.myawesomeproject.elements

import java.util.*

object Notifs {
    //9
    var rnd: Random = Random()
    var from0_to10: Array<String> = arrayOf("Элис Тэн-Робертс уже к полутора годам знала все столицы мира, а чего добился ты?")
    var from10_to20: Array<String> = arrayOf("Брайан Циммерман уже в 11 лет стал мэром, представляешь??",
            "Луи Брайль в 13 лет придумал самый удобный шрифт для слепых, которым пользуются по сей день",
            "Не каждый студент-второкурсник может похвастаться, что им интересовались Facebook и Google. Обе компании предложили Бену Пастернаку (в 16-то лет) у них работать, после того, как его игра Impossible Rush набрала на App Store 500 000 закачек за шесть недель после релиза.")
    var from20_to30: Array<String> = arrayOf("В 22 года Уолт Дисней создал небольшую анимационную студию (угадай какую)",
            "В 23 года Исаак Ньютон открыл Закон всемирного тяготения, а ты только пачки чипсов открываешь",
            "В 26 лет Марк Цукерберг был признан одним из самых молодых миллиардеров в мире")
    var from30_to40: Array<String> = arrayOf("В 32 Джоан Роулинг опубликовала свою первую книгу о Гарри Поттере с начальным тиражом в 1к экземпляров",
            "В 35 лет Уильям Боинг создал свой первый самолёт и поднял его в воздух :)",
            "В 39 лет Вера Вонг, 17 лет проработавшая редактором в журнале Vogue, начала создавать свадебные платья, которые сейчас знамениты на весь мир.")
    var from40_to50: Array<String> = arrayOf("В 41 год Христофор Колумб открыл Америку",
            "В 44 года Сэм Уолтон открыл первый магазин Walmart и стал одним из самых богатых людей в мире. До этого все его магазины терпели крах",
            "В 49 лет немецкий сапожник Адольф Дасслер основал Adidas")
    var from50_to60: Array<String> = arrayOf("В 54 года актёр Лесли Нильсен открыл в себе комедийный талант, благодаря которому прославился на весь мир",
            "В 58 лет Мигель де Сервантес опубликовал \"Дон Кихота\". До этого он искал работу, способную его прокормить")
    var from60_to70: Array<String> = arrayOf("В 68 лет канадский педагого, драматург и писатель Робертсон Дэвис начал писать, после того как вышел на пенсию. " +
            "Он опубликовал роман \"Мятежные ангелы\" и был номинирован на Нобелевскую премию")
    var from70_to80: Array<String> = arrayOf("Да думаю можно и отдохнуть")
    var motivation: Array<String> = arrayOf("А часики-то тикают",
            "А вот Ленка из пятого подъезда..",
            "Ну давай ещё посидим, подождём, мы же никуда не торопимся",
            "И помни: успех в трудностях", "Жизнь идёт, а ты нет", "Тебе не стыдно?",
            "Начать никогда не поздно, поздно не начать сейчас", "Предлагаю не сдаваться")

    fun getFrom0_to10(): String? {
        return if (rnd.nextBoolean()) motivation.get(rnd.nextInt(motivation.size)) else from0_to10.get(rnd.nextInt(from0_to10.size))
    }

    fun getFrom10_to20(): String? {
        return if (rnd.nextBoolean()) from10_to20.get(rnd.nextInt(from10_to20.size)) else getFrom0_to10()
    }

    fun getFrom20_to30(): String? {
        return if (rnd.nextBoolean()) from20_to30.get(rnd.nextInt(from20_to30.size)) else getFrom10_to20()
    }

    fun getFrom30_to40(): String? {
        return if (rnd.nextBoolean()) from30_to40.get(rnd.nextInt(from30_to40.size)) else getFrom20_to30()
    }

    fun getFrom40_to50(): String? {
        return if (rnd.nextBoolean()) from40_to50.get(rnd.nextInt(from40_to50.size)) else getFrom30_to40()
    }

    fun getFrom50_to60(): String? {
        return if (rnd.nextBoolean()) from50_to60.get(rnd.nextInt(from50_to60.size)) else getFrom40_to50()
    }

    fun getFrom60_to70(): String? {
        return if (rnd.nextBoolean()) from60_to70.get(rnd.nextInt(from60_to70.size)) else getFrom50_to60()
    }

    fun getFrom70_to80(): String? {
        return if (rnd.nextBoolean()) from70_to80.get(rnd.nextInt(from70_to80.size)) else getFrom60_to70()
    }

    fun getMotiv(age: Int): String? {
        return if (age >= 70) getFrom70_to80() else if (age >= 60) getFrom60_to70() else if (age >= 50) getFrom50_to60() else if (age >= 40) getFrom40_to50() else if (age >= 30) getFrom30_to40() else if (age >= 20) getFrom20_to30() else if (age >= 10) getFrom10_to20() else getFrom0_to10()
    }
}