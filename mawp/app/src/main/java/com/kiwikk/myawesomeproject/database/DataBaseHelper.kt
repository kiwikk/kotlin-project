package com.kiwikk.myawesomeproject.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.kiwikk.myawesomeproject.person.Person

class DataBaseHelper(context: Context?) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL("CREATE TABLE PERSON ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "NAME TEXT, "
                + "DATE_OF_BIRTH TEXT);")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}

    fun insertPerson(person: Person, db: SQLiteDatabase) {
        val contentValues = ContentValues()

        contentValues.put(ID, person._id)
        contentValues.put(NAME, person.name)
        contentValues.put(DATE_OF_BIRTH, person.birthDate)

        Log.d("Colums:", "id: ${person._id}, name: ${person.name}, bDate: ${person.birthDate}" )

        db.insert("PERSON", null, contentValues)
    }

    fun updateDB(person: Person, db: SQLiteDatabase, column: String?) {
        val contentValues = ContentValues()

        if (column == NAME) {
            contentValues.put(column, person.name)
        } else contentValues.put(column, person.birthDate)

        Log.d("id", "_id=" + person._id.toString())
        db.update("PERSON",
                contentValues,
                "_id=${person._id}",null)
    }

    companion object {
        private val DB_NAME: String = "Person"
        private const val DB_VERSION = 1
        val ID = "_id"
        val NAME = "NAME"
        val DATE_OF_BIRTH = "DATE_OF_BIRTH"
    }

}