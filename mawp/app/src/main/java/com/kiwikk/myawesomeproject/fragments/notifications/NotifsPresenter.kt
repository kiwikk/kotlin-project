package com.kiwikk.myawesomeproject.fragments.notifications

import com.kiwikk.myawesomeproject.MainActivity
import com.kiwikk.myawesomeproject.elements.Notifs
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class NotifsPresenter : MvpPresenter<NotifsView>() {
    val motivs = ArrayList<String>()
    var count = 0

    @Inject
    lateinit var notifInteractor: NotifsInteractor

    init {
        MainActivity.component.inject(this)
    }


    override fun onFirstViewAttach() {
        viewState.addNews(getNews())
    }

    fun getNews(): String {
        motivs.add(Notifs.getMotiv(notifInteractor.getPersonAge())!!)
        return motivs.last()
    }

    fun addNews() {
        viewState.addNews(getNews())
    }
}