package com.kiwikk.myawesomeproject.elements

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.fragments.home.HomeInteractor
import java.time.LocalDate

class WeekCard(var weekButton: WeekButton) : BottomSheetDialogFragment() {
    var input: EditText? = null
    var tasksLayout: LinearLayout? = null
    var switchRow: TableRow? = null
    var switchTextRow: TableRow? = null
    var myDate: LocalDate? = null
    lateinit var homeInteractor: HomeInteractor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.bottom_sheet_layout, container, false)
        homeInteractor = HomeInteractor()
        myDate = homeInteractor.person!!.birthDateLocal.plusWeeks(weekButton.getID().toLong())
        return view
    }

    override fun onStart() {
        super.onStart()
        createDays()
        setMonth()
        addTask()
    }

    private fun addTask() {
        val createButton = view!!.findViewById<ImageButton?>(R.id.addButton)
        createButton!!.setOnClickListener(View.OnClickListener {
            val alertBuilder = AlertDialog.Builder(context)
            alertBuilder.setTitle("Добавляем план на неделю")
            input = EditText(context)
            alertBuilder.setView(input)
            val task = arrayOfNulls<String?>(1)
            alertBuilder.setPositiveButton("Продолжим") { dialog, which ->
                task[0] = input!!.getText().toString()
                createTask(task[0])
            }
            val ad = alertBuilder.create()
            ad.show()
        })
    }

    private fun createTask(text: String?) {
        tasksLayout = view!!.findViewById<LinearLayout?>(R.id.tasksLayout)
        val task = Task(context, view)
        task.setText(text)
        tasksLayout!!.addView(task)
    }

    private fun setMonth() {
        val textView = view!!.findViewById<TextView?>(R.id.dateTextView)
        val month = StringBuilder()
        month.append(myDate!!.getMonth())
        if (myDate!!.getMonth() != myDate!!.plusDays(7).month) month.append(" - ").append(myDate!!.plusDays(7).month)
        month.append(" ").append(myDate!!.getYear())
        textView!!.setText(month.toString())
    }

    private fun createDays() {
        switchRow = view!!.findViewById<TableRow?>(R.id.switchTableRow)
        switchTextRow = view!!.findViewById<TableRow?>(R.id.switchTextTableRow)
        val pr = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        pr.setMargins(50, 0, 0, 0)
        switchTextRow!!.setLayoutParams(pr)
        val p = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        p.setMargins(0, -110, 0, 0)
        switchRow!!.setLayoutParams(p)
        val params = TableRow.LayoutParams(130, 130)
        for (i in 0..6) {
            val daySwitch = DaySwitch(this.context!!, i)
            daySwitch.layoutParams = params
            if (myDate!!.plusDays(i.toLong()).isAfter(LocalDate.now())) {
                daySwitch.isEnabled = false
            }
            val textView = TextView(this.context)
            textView.text = Integer.toString(myDate!!.plusDays(i.toLong()).dayOfMonth)
            textView.layoutParams = params
            switchTextRow!!.addView(textView)
            switchRow!!.addView(daySwitch)
        }
    }

}