package com.kiwikk.myawesomeproject.fragments.settings

import com.kiwikk.myawesomeproject.MainActivity
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class SettingsPresenter : MvpPresenter<SettingsView>() {
    @Inject
    lateinit var interactor: SettingsInteractor

    init {
        MainActivity.component.inject(this)
    }

    fun getNewName() {
        viewState.setNewName()
    }

    fun setNewBDate() {
        viewState.setNewBDate()
    }


    fun showDonut() {
        viewState.showDonut()
    }

    fun hideDonut() {
        viewState.hideDonnut()
    }

    fun updateName(name: String) {
        interactor.updateName(name)
    }

    fun updateBDate(bDate: String) {
        interactor.updateBDate(bDate)
    }

    fun getPersonName(): String {
        return interactor.getPersonName()
    }
}