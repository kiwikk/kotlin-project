package com.kiwikk.myawesomeproject.fragments.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.elements.New
import com.kiwikk.myawesomeproject.fragments.home.HomePresenter
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

/**
 * A simple [Fragment] subclass.
 * Use the [NotificationsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NotificationsFragment : MvpAppCompatFragment(), NotifsView {

    private lateinit var addButton: Button
    private var vview: View? = null
    private lateinit var linearLayout: LinearLayout

    @InjectPresenter
    lateinit var nPresenter: NotifsPresenter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        vview = inflater.inflate(R.layout.fragment_notifications, container, false)

        linearLayout = vview!!.findViewById(R.id.notifsLayout)
        //кнопка на будущее
//        addButton = vview!!.findViewById(R.id.moreMotivation)
//        addButton.setOnClickListener { nPresenter.addNews() }


        return vview
    }

    //заюзать в recyclerView
    override fun addNews(text: String) {
        //TODO: вынести в даггер
        val news = New(context, view)
        news.setText(text)
        linearLayout.addView(news)
    }

    companion object {
        private val ARG_PARAM1: String = "param1"
        private val ARG_PARAM2: String = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NotificationsFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String?, param2: String?): NotificationsFragment? {
            val fragment = NotificationsFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}