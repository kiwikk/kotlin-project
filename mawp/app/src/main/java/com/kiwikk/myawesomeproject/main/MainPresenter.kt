package com.kiwikk.myawesomeproject.main

import moxy.InjectViewState
import moxy.MvpPresenter

//презентор не должен создавать фрагменты
//навигацией занимается роутер, это интерфейс с методами гоу ту....
//но это потом
//этим должен пользоваться презентер
//по-хорошему не должно быть андроидовских классов

@InjectViewState
class MainPresenter: MvpPresenter<MainView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        beginHomeTransaction()
    }

    fun beginHomeTransaction(){
        viewState.beginHomeTransaction()
    }

    fun beginNotifsTransaction(){
        viewState.beginNotifsTransaction()
    }

    fun beginSettingsTransaction(){
        viewState.beginSettingsTransaction()
    }
}